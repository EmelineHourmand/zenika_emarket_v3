FROM eclipse-temurin:17
RUN mkdir /opt/app
COPY target/Zenika_eMarket_v3-0.0.1-SNAPSHOT.jar /opt/app/Zenika_eMarket.jar
CMD ["java", "-jar", "/opt/app/Zenika_eMarket.jar"]