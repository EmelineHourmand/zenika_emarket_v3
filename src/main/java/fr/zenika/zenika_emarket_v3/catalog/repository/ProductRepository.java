package fr.zenika.zenika_emarket_v3.catalog.repository;

import fr.zenika.zenika_emarket_v3.catalog.domain.Product;
import fr.zenika.zenika_emarket_v3.catalog.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Emeline Hourmand
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Integer>, JpaSpecificationExecutor<Product> {
    List<Product> getProductByName(String name);
    List<Product> getProductByCategory(Category category);
}
