package fr.zenika.zenika_emarket_v3.catalog.repository;

import fr.zenika.zenika_emarket_v3.catalog.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author Emeline Hourmand
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer>, JpaSpecificationExecutor<Category> {
    Category getCategoryByName(String name);
}
