package fr.zenika.zenika_emarket_v3.catalog.filter;

import fr.zenika.zenika_emarket_v3.catalog.domain.Category;
import fr.zenika.zenika_emarket_v3.catalog.filter.CategoryFilterDto;
import org.springframework.data.jpa.domain.Specification;

import static fr.zenika.zenika_emarket_v3.utils.SpecificationUtils.hasAttributeEquals;
import static fr.zenika.zenika_emarket_v3.utils.SpecificationUtils.hasAttributeLike;

/**
 * @author Emeline Hourmand
 */
public class CategorySpecifications {

    public static Specification<Category> fromFilter(CategoryFilterDto filter) {
        return hasName(filter.getName())
                .and(hasNameLike(filter.getNameLike()));
    }

    public static Specification<Category> hasName(String name) {
        return hasAttributeEquals("name", name);
    }

    public static Specification<Category> hasNameLike(String name) {
        return hasAttributeLike("name", name);
    }
}
