package fr.zenika.zenika_emarket_v3.catalog.filter;

import lombok.Data;

/**
 * @author Emeline Hourmand
 */
@Data
public class ProductFilterDto {

    private String name;
    private String nameLike;
    private Double taxInclPriceMin;
    private Double taxInclPriceMax;
    private Double wtPriceMin;
    private Double wtPriceMax;

}
