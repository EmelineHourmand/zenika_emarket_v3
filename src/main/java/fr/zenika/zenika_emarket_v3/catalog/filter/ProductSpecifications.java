package fr.zenika.zenika_emarket_v3.catalog.filter;

import fr.zenika.zenika_emarket_v3.catalog.domain.Product;
import org.springframework.data.jpa.domain.Specification;

import static fr.zenika.zenika_emarket_v3.utils.SpecificationUtils.*;

/**
 * @author Emeline Hourmand
 */
public class ProductSpecifications {

    public static Specification<Product> fromFilter(ProductFilterDto filter) {
        return hasName(filter.getName())
                .and(hasNameLike(filter.getNameLike()))
                .and(hasWtPriceMin(filter.getWtPriceMin()))
                .and(hasWtPriceMax(filter.getWtPriceMax()))
                .and(hasPriceInclTaxMin(filter.getTaxInclPriceMin()))
                .and(hasPrinceInclTaxMax(filter.getTaxInclPriceMax()));
    }

    public static Specification<Product> hasName(String name) {
        return hasAttributeEquals("name", name);
    }

    public static Specification<Product> hasNameLike(String name) {
        return hasAttributeLike("name", name);
    }

    public static Specification<Product> hasPriceInclTaxMin(Double taxInclPriceMin) {
        return hasAttributeGreaterThanOrEqualsTo("taxInclPrice", taxInclPriceMin);
    }

    public static Specification<Product> hasPrinceInclTaxMax(Double taxInclPriceMax) {
        return hasAttributeLessThan("taxInclPrice", taxInclPriceMax);
    }

    public static Specification<Product> hasWtPriceMin(Double wtPriceMin) {
        return hasAttributeGreaterThanOrEqualsTo("wtPrice", wtPriceMin);
    }

    public static Specification<Product> hasWtPriceMax(Double wtPriceMax) {
        return hasAttributeLessThan("wtPrice", wtPriceMax);
    }
}
