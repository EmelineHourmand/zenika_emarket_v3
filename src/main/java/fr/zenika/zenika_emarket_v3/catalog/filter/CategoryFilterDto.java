package fr.zenika.zenika_emarket_v3.catalog.filter;

import lombok.Data;

/**
 * @author Emeline Hourmand
 */
@Data
public class CategoryFilterDto {

    private String name;
    private String nameLike;

}
