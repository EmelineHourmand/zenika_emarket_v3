package fr.zenika.zenika_emarket_v3.catalog.domain;

import fr.zenika.zenika_emarket_v3.catalog.exception.ProductInvalidException;
import org.springframework.util.StringUtils;

import javax.persistence.*;

/**
 * @author Emeline Hourmand
 */
@Entity
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_product")
    private Integer id;
    private String name;
    private Double taxInclPrice;
    private Double wtPrice;
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_category")
    private Category category;

    public Product() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (!StringUtils.hasText(name)) {
            throw new ProductInvalidException("name");
        }
        this.name = name;
    }

    public Double getTaxInclPrice() {
        return taxInclPrice;
    }

    public void setTaxInclPrice(Double taxInclPrice) {
        if (taxInclPrice == null) {
            throw new ProductInvalidException("taxInclPrice");
        }
        this.taxInclPrice = taxInclPrice;
    }

    public Double getWtPrice() {
        return wtPrice;
    }

    public void setWtPrice(Double wtPrice) {
        if (wtPrice == null) {
            throw new ProductInvalidException("wtPrice");
        }
        this.wtPrice = wtPrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
