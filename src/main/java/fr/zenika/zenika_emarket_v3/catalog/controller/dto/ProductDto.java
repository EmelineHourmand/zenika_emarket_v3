package fr.zenika.zenika_emarket_v3.catalog.controller.dto;

import fr.zenika.zenika_emarket_v3.catalog.domain.Product;
import lombok.Data;

/**
 * @author Emeline Hourmand
 */
@Data
public class ProductDto {

    private Integer id;
    private String name;
    private double price;
    private String description;
    private String categoryName;
    private Integer categoryId;

    /**
     * Permet de convertir un Product en ProductDto
     * @param product à convertir
     * @return un ProductDto
     */
    public static ProductDto fromDomain(Product product) {

        ProductDto productDto = new ProductDto();

        productDto.setId(product.getId());
        productDto.setName(product.getName());
        productDto.setPrice(product.getTaxInclPrice());
        productDto.setDescription(product.getDescription());
        productDto.setCategoryName(product.getCategory().getName());
        productDto.setCategoryId(product.getCategory().getId());

        return productDto;
    }

    /**
     * Permet de convertir un ProductDto en Product
     * @param productDto à convertir
     * @return un Product
     */
    public static Product toDomain(ProductDto productDto) {

        Product product = new Product();

        product.setId(productDto.getId());
        product.setName(productDto.getName());
        product.setTaxInclPrice(productDto.getPrice());
        product.setDescription(productDto.getDescription());

        return product;
    }

}
