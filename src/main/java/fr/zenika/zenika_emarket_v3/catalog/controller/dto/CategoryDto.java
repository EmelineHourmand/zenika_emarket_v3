package fr.zenika.zenika_emarket_v3.catalog.controller.dto;

import fr.zenika.zenika_emarket_v3.catalog.domain.Category;
import fr.zenika.zenika_emarket_v3.catalog.domain.Product;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Emeline Hourmand
 */
@Data
public class CategoryDto {

    private Integer id;
    private String name;
    private List<ProductDto> productDtos = new ArrayList<>();

    /**
     * Permet de convertir une Category en CategoryDto
     * @param category à convertir
     * @return une CategoryDto
     */
    public static CategoryDto fromDomain(Category category) {
        CategoryDto categoryDto = new CategoryDto();

        categoryDto.setId(category.getId());
        categoryDto.setName(category.getName());

        List<ProductDto> productDtos  = new ArrayList<>();
        for (Product product : category.getProducts()) {
            productDtos.add(ProductDto.fromDomain(product));
        }

        categoryDto.setProductDtos(productDtos);

        return categoryDto;
    }

    /**
     * Permet de convetir une CategoryDto en Category
     * @param categoryDto à convetir
     * @return une Category
     */
    public static Category toDomain(CategoryDto categoryDto) {
        Category category = new Category();

        category.setId(categoryDto.getId());
        category.setName(categoryDto.getName());

        List<Product> products = new ArrayList<>();
        for (ProductDto productDto : categoryDto.getProductDtos()) {
            products.add(ProductDto.toDomain(productDto));
        }

        category.setProducts(products);

        return category;
    }

}
