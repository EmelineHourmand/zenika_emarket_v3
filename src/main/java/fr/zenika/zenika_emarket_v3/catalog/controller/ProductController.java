package fr.zenika.zenika_emarket_v3.catalog.controller;

import fr.zenika.zenika_emarket_v3.catalog.domain.Product;
import fr.zenika.zenika_emarket_v3.catalog.filter.ProductFilterDto;
import fr.zenika.zenika_emarket_v3.catalog.filter.ProductSpecifications;
import fr.zenika.zenika_emarket_v3.utils.PageDto;
import fr.zenika.zenika_emarket_v3.catalog.service.ProductService;
import fr.zenika.zenika_emarket_v3.catalog.controller.dto.ProductAdminDto;
import fr.zenika.zenika_emarket_v3.catalog.controller.dto.ProductDto;
import lombok.Data;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.SortDefault;
import org.springframework.web.bind.annotation.*;

/**
 * @author Emeline Hourmand
 */
@Data
@RestController
@CrossOrigin
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;


    /**
     * Permet de récupérer les products
     * @param pageable les instructions pour retourner les products
     * @return la liste de Product paginé
     */
    @GetMapping
    public PageDto<ProductDto> getAllProducts(@SortDefault(value = {"id"}) Pageable pageable,
                                              ProductFilterDto filter) {
            Specification<Product> productSpecification = ProductSpecifications.fromFilter(filter);
            return PageDto.fromDomain(
                    productService.getProducts(pageable, productSpecification),
                    ProductDto::fromDomain);
    }

    /**
     * Permet de récupérer un Product par son attribue Id
     * @param id du Product
     * @return un ProductDto
     */
    @GetMapping("/{id}")
    public ProductDto getProductById(@PathVariable Integer id) {
        return ProductDto.fromDomain(productService.getProductById(id));
    }

    /**
     * Permet d'ajouter un Product
     * @param productAdminDto à ajouter
     * @return un ProductDto
     */
    @PostMapping
    public ProductDto saveProduct(@RequestBody ProductAdminDto productAdminDto) {
        return ProductDto.fromDomain(
                productService.addProduct(ProductAdminDto.toDomain(productAdminDto)));
    }

    /**
     * Permet de modifier un Product
     * @param id du Product
     * @param updatedProduct Product modifié
     * @return un ProductDto
     */
    @PutMapping("/{id}")
    public ProductDto updateProduct(@PathVariable Integer id, @RequestBody ProductAdminDto updatedProduct) {
        updatedProduct.setId(id);
        return ProductDto.fromDomain(
                productService.updateProduct(id, ProductAdminDto.toDomain(updatedProduct)));
    }

    /**
     * Supprimer un Product
     * @param id du Product
     */
    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable Integer id) {
        productService.deleteProduct(id);
    }

}
