package fr.zenika.zenika_emarket_v3.catalog.controller.dto;

import fr.zenika.zenika_emarket_v3.catalog.domain.Product;
import fr.zenika.zenika_emarket_v3.catalog.domain.Category;
import lombok.Data;

/**
 * @author Emeline Hourmand
 */
@Data
public class ProductAdminDto {

    private Integer id;
    private String name;
    private Double wtPrice;
    private Double taxInclPrice;
    private String description;
    private Integer categoryId;

    /**
     * Permet de convertir un ProductAdminDto en Product
     * @param productAdminDto à convertir
     * @return un Product
     */
    public static Product toDomain(ProductAdminDto productAdminDto) {

        Product product = new Product();

        if (productAdminDto.getId() != null) {
            product.setId(productAdminDto.getId());
        }
        if (productAdminDto.getName() != null) {
            product.setName(productAdminDto.getName());
        }
        if (productAdminDto.getDescription() != null) {
            product.setDescription(productAdminDto.getDescription());
        }
        if (productAdminDto.getWtPrice() != null) {
            product.setWtPrice(productAdminDto.getWtPrice());
        }
        if (productAdminDto.getTaxInclPrice() != null) {
            product.setTaxInclPrice(productAdminDto.getTaxInclPrice());
        }

        if (productAdminDto.getCategoryId() != null) {
            Category category = new Category();
            category.setId(productAdminDto.getCategoryId());
            product.setCategory(category);
        }

        return product;
    }
}
