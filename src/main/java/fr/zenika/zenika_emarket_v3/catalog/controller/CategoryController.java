package fr.zenika.zenika_emarket_v3.catalog.controller;

import fr.zenika.zenika_emarket_v3.catalog.controller.dto.CategoryDto;
import fr.zenika.zenika_emarket_v3.catalog.domain.Category;
import fr.zenika.zenika_emarket_v3.catalog.filter.CategoryFilterDto;
import fr.zenika.zenika_emarket_v3.catalog.service.CategoryService;
import fr.zenika.zenika_emarket_v3.utils.PageDto;
import fr.zenika.zenika_emarket_v3.catalog.service.ProductService;
import lombok.Data;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.SortDefault;
import org.springframework.web.bind.annotation.*;

import static fr.zenika.zenika_emarket_v3.catalog.filter.CategorySpecifications.fromFilter;

/**
 * @author Emeline Hourmand
 */
@Data
@RestController
@CrossOrigin
@RequestMapping("/categories")
public class CategoryController {

    private final CategoryService categoryService;
    private final ProductService productService;

    /**
     * Permet de récupérer la liste des Categories
     * @return une liste de CategoryDto
     */
    @GetMapping
    public PageDto<CategoryDto> getAll(@SortDefault(value = {"name"}) Pageable pageable,
                                    CategoryFilterDto filter) {
        Specification<Category> specification = fromFilter(filter);
        return PageDto.fromDomain(
                categoryService.getCategories(pageable, specification),
                CategoryDto::fromDomain);
    }

    /**
     * Permet de récupérer une Category par son attribue Id
     * @param id de la Category
     * @return une CategoryDto
     */
    @GetMapping("/{id}")
    public CategoryDto getById(@PathVariable Integer id) {
        return CategoryDto.fromDomain(categoryService.getCategoryById(id));
    }

    /**
     * Permet d'ajouter une nouvelle Category
     * @param categoryDto nouvelle Category
     * @return une CateogoryDto
     */
    @PostMapping
    private CategoryDto saveCategory(@RequestBody CategoryDto categoryDto) {
        return CategoryDto.fromDomain(categoryService.addCategory(CategoryDto.toDomain(categoryDto)));
    }

    /**
     * Permet de modifier une Category
     * @param id de la Category
     * @param categoryDto les modifications
     * @return une CategoryDto
     */
    @PutMapping("/{id}")
    private CategoryDto updateCategory(@PathVariable Integer id, @RequestBody CategoryDto categoryDto) {
        return CategoryDto.fromDomain(categoryService.updateCategory(id, CategoryDto.toDomain(categoryDto)));
    }

    /**
     * Permet de supprimer une Category
     * @param id de la Category
     */
    @DeleteMapping("/{id}")
    private void deleteCategory(@PathVariable Integer id) {
        categoryService.deleteCategory(id);
    }

}
