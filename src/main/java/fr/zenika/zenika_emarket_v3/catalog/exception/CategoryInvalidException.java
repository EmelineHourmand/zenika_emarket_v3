package fr.zenika.zenika_emarket_v3.catalog.exception;

import fr.zenika.zenika_emarket_v3.exception.InvalidException;

/**
 * @author Emeline Hourmand
 */
public class CategoryInvalidException extends InvalidException {

    public CategoryInvalidException(String attributeName) {
        super("Category", attributeName);
    }
}
