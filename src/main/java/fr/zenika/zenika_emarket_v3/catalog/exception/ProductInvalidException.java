package fr.zenika.zenika_emarket_v3.catalog.exception;

import fr.zenika.zenika_emarket_v3.exception.InvalidException;

/**
 * @author Emeline Hourmand
 */
public class ProductInvalidException extends InvalidException  {

    public ProductInvalidException(String attributeName) {
        super("Product", attributeName);
    }
}
