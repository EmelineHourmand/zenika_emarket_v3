package fr.zenika.zenika_emarket_v3.catalog.exception;

import fr.zenika.zenika_emarket_v3.exception.NotFoundException;

/**
 * @author Emeline Hourmand
 */
public class ProductNotFound extends NotFoundException {

    /**
     * Information sur le ProductNotFound
     * @param id du ProductNotFound
     */
    public ProductNotFound(Integer id) {
        super(id, "Product");
    }
}
