package fr.zenika.zenika_emarket_v3.catalog.exception;

import fr.zenika.zenika_emarket_v3.exception.ErrorHttp;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author Emeline Hourmand
 */
@ControllerAdvice
@Order(2)
public class CategoryExceptionHandlerController {

    /**
     * Permet de gérer l'exception lorsqu'une Category n'existe pas.
     * @param e un IdNotFoundException
     * @return une Err 404
     */
    @ExceptionHandler(CategoryNotFound.class)
    public ResponseEntity<ErrorHttp> onIdNotFoundException(CategoryNotFound e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                new ErrorHttp(e.getTimestamp() + " : " +
                        e.getEntityName() + " with id " +
                        e.getId() + " not found"));
    }
}
