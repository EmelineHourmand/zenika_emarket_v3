package fr.zenika.zenika_emarket_v3.catalog.exception;

import fr.zenika.zenika_emarket_v3.exception.NotFoundException;

/**
 * @author Emeline Hourmand
 */
public class CategoryNotFound extends NotFoundException {

    /**
     * Information sur le CategoryByIdNotFound
     * @param id du CategoryNotFound
     */
    public CategoryNotFound(Integer id) {
        super(id, "Category");
    }
}
