package fr.zenika.zenika_emarket_v3.catalog.exception;

import fr.zenika.zenika_emarket_v3.exception.ErrorHttp;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author Emeline Hourmand
 */
@Order(1)
@ControllerAdvice
public class ProductExceptionHandlerController {

    /**
     * Permet de gérer l'exception lorsqu'un Product n'existe pas.
     * @param e un IdNotFoundException
     * @return une Err 404
     */
    @ExceptionHandler(ProductNotFound.class)
    public ResponseEntity<ErrorHttp> onIdNotFoundException(ProductNotFound e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                new ErrorHttp(e.getTimestamp() + " : " +
                        e.getEntityName() + " with id " +
                        e.getId() + " not found."));
    }

    /**
     * Permet de gérer l'exception lorsqu'un attribut de Product est incorrecte
     * @param e un ProductInvalidException
     * @return une Err 400
     */
    @ExceptionHandler(ProductInvalidException.class)
    public ResponseEntity<ErrorHttp> onInvalidException(ProductInvalidException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                new ErrorHttp(e.getTimestamp() + " : " +
                        e.getEntityName() + "." +
                        e.getAttributeName() + " is empty."));

    }

}
