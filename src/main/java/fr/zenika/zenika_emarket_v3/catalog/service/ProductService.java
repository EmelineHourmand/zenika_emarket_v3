package fr.zenika.zenika_emarket_v3.catalog.service;

import fr.zenika.zenika_emarket_v3.catalog.domain.Product;
import fr.zenika.zenika_emarket_v3.catalog.domain.Category;
import fr.zenika.zenika_emarket_v3.catalog.exception.CategoryInvalidException;
import fr.zenika.zenika_emarket_v3.catalog.repository.CategoryRepository;
import fr.zenika.zenika_emarket_v3.catalog.exception.ProductNotFound;
import fr.zenika.zenika_emarket_v3.catalog.repository.ProductRepository;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @author Emeline Hourmand
 */
@Data
@Service
public class ProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;

    /**
     * Permet de récupérer une liste de Product depuis la BDD
     * @return une liste de Product
     */
    @Transactional
    public Page<Product> getProducts(Pageable pageable, Specification<Product> productSpecification) {
        return productRepository.findAll(productSpecification, pageable);
    }

    /**
     * Permet de récupérer un Product par son Id dans la BDD
     * @param idProduct du Product
     * @return un Product
     */
    @Transactional
    public Product getProductById(Integer idProduct) {
        return productRepository.findById(idProduct).
        orElseThrow(() -> new ProductNotFound(idProduct));
    }

    /**
     * Permet d'ajouter un Product dans la BDD
     * @param product à ajouter
     * @return un product
     */
    @Transactional
    public Product addProduct(Product product) {
        productRepository.save(product);
        return product;
    }

    /**
     * Permet de mettre à jour un Product dans la BDD
     * @param id du Product
     * @param updatedProduct les modifications
     * @return un Product
     */
    @Transactional
    public Product updateProduct(Integer id, Product updatedProduct) {
        Product product = getProductById(id);
        if (updatedProduct.getName() != null) {
            product.setName(updatedProduct.getName());
        }
        if (updatedProduct.getDescription() != null) {
            product.setDescription(updatedProduct.getDescription());
        }
        if (updatedProduct.getTaxInclPrice() != null) {
            product.setTaxInclPrice(updatedProduct.getTaxInclPrice());
        }
        if (updatedProduct.getCategory() != null) {
            Category category = categoryRepository.findById(updatedProduct.getCategory().getId())
                    .orElseThrow(()-> new CategoryInvalidException("id"));
            product.setCategory(category);
        }
        return product;
    }

    /**
     * Permet de supprimer un Product de la BDD
     * @param id du Product
     */
    public void deleteProduct(Integer id) {
        productRepository.delete(getProductById(id));
    }
}
