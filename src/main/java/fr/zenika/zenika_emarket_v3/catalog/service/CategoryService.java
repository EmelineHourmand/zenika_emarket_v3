package fr.zenika.zenika_emarket_v3.catalog.service;

import fr.zenika.zenika_emarket_v3.catalog.domain.Category;
import fr.zenika.zenika_emarket_v3.catalog.domain.Product;
import fr.zenika.zenika_emarket_v3.catalog.exception.CategoryNotFound;
import fr.zenika.zenika_emarket_v3.catalog.repository.CategoryRepository;
import fr.zenika.zenika_emarket_v3.catalog.repository.ProductRepository;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Emeline Hourmand
 */
@Data
@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;

    /**
     * Permet de récupérer la liste de Category depuis la BDD
     * @return une liste de Category
     */
    @Transactional
    public Page<Category> getCategories(Pageable pageable, Specification<Category> specification) {
        return categoryRepository.findAll(specification, pageable);
    }

    /**
     * Permet de récupérer une Category par son Id depuis la BDD
     * @param id de la Category
     * @return une Category
     */
    @Transactional
    public Category getCategoryById(Integer id) {
        return categoryRepository.findById(id).
                orElseThrow(() -> new CategoryNotFound(id));
    }

    /**
     * Permet d'ajouter une Category dans la BDD
     * @param category à ajouter
     * @return une Category
     */
    @Transactional
    public Category addCategory(Category category) {
        return categoryRepository.save(category);
    }

    /**
     * Permet de mettre à jour une Category dans la BDD
     * @param idCategory de la Category
     * @param updatedCategory les modifications
     * @return une Category
     */
    @Transactional
    public Category updateCategory(Integer idCategory, Category updatedCategory) {
        Category category = getCategoryById(idCategory);

        if (updatedCategory.getName() != null) {
            category.setName(updatedCategory.getName());
        }

        return category;
    }

    /**
     * Permet de supprimer une Category dans la BDD
     * @param idCategory de la Category
     */
    @Transactional
    public void deleteCategory(Integer idCategory) {

        Category defaultCategory = categoryRepository.getCategoryByName("Default");
        Category category = getCategoryById(idCategory);

        List<Product> products = productRepository.getProductByCategory(category);
        for (Product product : products) {
            product.setCategory(defaultCategory);
        }

        categoryRepository.delete(category);
    }

}