package fr.zenika.zenika_emarket_v3.exception;

/**
 * @author Emeline Hourmand
 */
public class ErrorHttp {

    private String message;

    public ErrorHttp() {
    }

    public ErrorHttp(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public ErrorHttp setMessage(String message) {
        this.message = message;
        return this;
    }
}
