package fr.zenika.zenika_emarket_v3.exception;

/**
 * @author Emeline Hourmand
 */
public class InvalidException extends FunctionalException {

    private String entityName;
    private String attributeName;

    public InvalidException(String entityName, String attributeName) {
        this.entityName = entityName;
        this.attributeName = attributeName;
    }

    public String getEntityName() {
        return entityName;
    }

    public String getAttributeName() {
        return attributeName;
    }
}
