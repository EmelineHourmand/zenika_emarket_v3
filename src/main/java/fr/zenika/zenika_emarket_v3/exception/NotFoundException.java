package fr.zenika.zenika_emarket_v3.exception;

/**
 * @author Emeline Hourmand
 */
public class NotFoundException extends FunctionalException {

    private Integer id;
    private String entityName;

    public NotFoundException(Integer id, String entityName) {
        this.id = id;
        this.entityName = entityName;
    }

    public Integer getId() {
        return id;
    }

    public String getEntityName() {
        return entityName;
    }

}
