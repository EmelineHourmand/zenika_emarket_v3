package fr.zenika.zenika_emarket_v3.exception;

import org.aspectj.weaver.ast.Not;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author Emeline Hourmand
 */
@ControllerAdvice
public class ExceptionHandlerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlerController.class);

    /**
     * Permet de gérer les errs globales
     * @param e l'exception
     * @return un Logger
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorHttp> onUnexpectedException(Exception e) {
        LOGGER.error("Exception error", e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorHttp("Unexpected error"));
    }

    @ExceptionHandler(FunctionalException.class)
    public ResponseEntity<ErrorHttp> onFunctionalException(FunctionalException e) {
        LOGGER.debug("Functional exception handling : ", e);
        return ResponseEntity.badRequest().body(new ErrorHttp(e.getMessage()));
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorHttp> onNotFoundException(NotFoundException e) {
        LOGGER.error("Not found exception handling", e);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorHttp(e.getMessage()));
    }

    @ExceptionHandler(InvalidException.class)
    public ResponseEntity<ErrorHttp> ontInvalidException(InvalidException e) {
        LOGGER.error("Invalid exception handling", e);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorHttp(e.getMessage()));
    }

}