package fr.zenika.zenika_emarket_v3.exception;

import java.time.Instant;

/**
 * @author Emeline Hourmand
 */
public class FunctionalException extends RuntimeException {

    private Instant timestamp;

    public FunctionalException() {
        this.timestamp = Instant.now();
    }

    public Instant getTimestamp() {
        return timestamp;
    }

}
