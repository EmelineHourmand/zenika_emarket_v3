package fr.zenika.zenika_emarket_v3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZenikaEMarketV3Application {

    public static void main(String[] args) {
        SpringApplication.run(ZenikaEMarketV3Application.class, args);
    }

}
