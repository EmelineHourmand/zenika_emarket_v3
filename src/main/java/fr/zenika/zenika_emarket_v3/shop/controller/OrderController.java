package fr.zenika.zenika_emarket_v3.shop.controller;

import fr.zenika.zenika_emarket_v3.shop.controller.dto.OrderDto;
import fr.zenika.zenika_emarket_v3.shop.service.OrderLineService;
import fr.zenika.zenika_emarket_v3.shop.service.OrderService;
import fr.zenika.zenika_emarket_v3.utils.PageDto;
import lombok.Data;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.SortDefault;
import org.springframework.web.bind.annotation.*;

/**
 * @author Emeline Hourmand
 */
@Data
@RestController
@CrossOrigin
@RequestMapping("/orders")
public class OrderController {

    private final OrderLineService orderLineService;
    private final OrderService orderService;

    /**
     * Permet de récupérer toutes les Order
     * @return une liste d'OrderDto
     */
    @GetMapping
    public PageDto<OrderDto> getAllOrder(@SortDefault(value = {"id"}) Pageable pageable) {
        return PageDto.fromDomain(orderService.getOrders(pageable), OrderDto::fromDomain);
    }

    /**
     * Permet de récupérer un Order par son Id
     * @param id de l'Order
     * @return un OrderDto
     */
    @GetMapping("/{id}")
    private OrderDto getById(@PathVariable Integer id) {
        return OrderDto.fromDomain(orderService.getOrderById(id));
    }

    /**
     * Permet d'ajouter un Order.
     * @param idBasket du Basket a convertir en Order
     * @param idCustomer du Customer à qui appartient l'Order
     * @return un OrderDto
     */
    @PostMapping("/baskets/{idBasket}/customers/{idCustomer}")
    private OrderDto saveOrder(@PathVariable Integer idBasket, @PathVariable Integer idCustomer) {
        return OrderDto.fromDomain(orderService.addOrder(idBasket, idCustomer));
    }

    /**
     * Permet de supprimer un Order
     * @param idOrder Order à supprimer.
     */
    @DeleteMapping("/{idOrder}")
    private void deleteOrder(@PathVariable Integer idOrder) {
        orderService.deleteOrder(idOrder);
    }

}
