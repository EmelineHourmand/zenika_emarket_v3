package fr.zenika.zenika_emarket_v3.shop.controller;

import fr.zenika.zenika_emarket_v3.shop.controller.dto.BasketDto;
import fr.zenika.zenika_emarket_v3.shop.controller.dto.BasketLineDto;
import fr.zenika.zenika_emarket_v3.shop.service.BasketLineService;
import fr.zenika.zenika_emarket_v3.shop.service.BasketService;
import lombok.Data;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * @author Emeline Hourmand
 */
@Data
@RestController
@CrossOrigin
@RequestMapping("/baskets")
public class BasketController {

    private final BasketLineService basketLineService;
    private final BasketService basketService;

    /**
     * Permet de récupérer la liste de Basket.
     * @return une liste de Basket
     */
    @GetMapping
    public List<BasketDto> getAll() {
        return basketService.getBaskets().stream()
                .map(BasketDto::fromDomain)
                .collect(toList());
    }

    /**
     * Permet de récupérer un Basket par son attribue Id
     * @param id du Basket
     * @return un Basket
     */
    @GetMapping("/{id}")
    public BasketDto getById(@PathVariable Integer id) {
        return BasketDto.fromDomain(basketService.getBasketById(id));
    }

    /**
     * Permet de créer un nouveau Basket
     * @param basketDto à créer
     * @return un BasketDto
     */
    @PostMapping
    private BasketDto saveBasket(@RequestBody BasketDto basketDto) {
        return BasketDto.fromDomain(basketService.addBasket(BasketDto.toDomain(basketDto)));
    }

    /**
     * Permet d'ajouter un Product dans un BasketLine
     * @param idBasket du Basket
     * @param idProduct du Product
     * @param basketLineDto BasketLine
     * @return un BasketDto
     */
    @PostMapping("/{idBasket}/products/{idProduct}")
    private BasketDto saveProductInBasket(@PathVariable Integer idBasket,
                                          @PathVariable Integer idProduct,
                                          @RequestBody BasketLineDto basketLineDto) {

        return BasketDto.fromDomain(
                basketLineService.addProductToBasket(idBasket, idProduct, basketLineDto.getQuantity()));
    }

    /**
     * Permet de supprimer un Basket
     * @param id du Basket
     */
    @DeleteMapping("/{id}")
    private void deleteBasket(@PathVariable Integer id) {
        basketService.deleteBasket(id);
    }

    /**
     * Permet de supprimer un Product dans un Basket
     * @param idBasket du Basket
     * @param idProduct du Product
     */
    @DeleteMapping("/{idBasket}/products/{idProduct}")
    private void deleteProductIntoBasket(@PathVariable Integer idBasket, @PathVariable Integer idProduct) {
        basketLineService.deleteProductIntoBasket(idBasket, idProduct);
    }

}
