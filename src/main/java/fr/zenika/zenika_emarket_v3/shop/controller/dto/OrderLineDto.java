package fr.zenika.zenika_emarket_v3.shop.controller.dto;

import fr.zenika.zenika_emarket_v3.shop.domain.OrderLine;
import fr.zenika.zenika_emarket_v3.catalog.controller.dto.ProductDto;
import lombok.Data;

/**
 * @author Emeline Hourmand
 */
@Data
public class OrderLineDto {

    private ProductDto productDto;
    private Integer quantity;
    private Double priceTaxInlc;
    private Double priceWtax;

    /**
     * Permet de convertir un OrderLine en OrderLineDto
     * @param line OrderLine à convertir
     * @return OrderLineDto
     */
    public static OrderLineDto fromDomain(OrderLine line) {
        OrderLineDto orderLineDto = new OrderLineDto();

        orderLineDto.setProductDto(ProductDto.fromDomain(line.getProducts()));
        orderLineDto.setQuantity(line.getQuantity());
        orderLineDto.setPriceTaxInlc(line.getPriceTaxInlc());
        orderLineDto.setPriceWtax(line.getPriceWtax());

        return orderLineDto;
    }

    /**
     * Permet de convertir un OrderLineDto en OrderLine
     * @param lines OrderLineDto à convertir
     * @return un OrderLine
     */
    public static OrderLine toDomain(OrderLineDto lines) {
        OrderLine orderLine = new OrderLine();

        orderLine.setProducts(ProductDto.toDomain(lines.getProductDto()));
        orderLine.setQuantity(lines.getQuantity());
        orderLine.setPriceTaxInlc(lines.getPriceTaxInlc());
        orderLine.setPriceWtax(lines.getPriceWtax());

        return orderLine;
    }
}
