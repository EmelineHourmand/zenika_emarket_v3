package fr.zenika.zenika_emarket_v3.shop.controller.dto;

import fr.zenika.zenika_emarket_v3.shop.domain.Customer;
import fr.zenika.zenika_emarket_v3.shop.domain.Order;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Emeline Hourmand
 */
@Data
public class CustomerDto {

    private Integer id;
    private String firstName;
    private String lastName;
    private String userName;
    private BasketDto basketDto;
    private List<OrderDto> orderDtos;

    /**
     * Permet de convertir un Customer en CustomerDto
     * @param customer à convertir
     * @return un CustomerDto
     */
    public static CustomerDto fromDomain(Customer customer) {

        CustomerDto customerDto = new CustomerDto();

        customerDto.setId(customer.getId());
        customerDto.setFirstName(customer.getFirstName());
        customerDto.setLastName(customer.getLastName());
        customerDto.setUserName(customer.getUserName());
        customerDto.setBasketDto(BasketDto.fromDomain(customer.getBasket()));

        List<OrderDto> orderDtoList = new ArrayList<>();
        for (Order order : customer.getOrders()) {
            orderDtoList.add(OrderDto.fromDomain(order));
        }

        customerDto.setOrderDtos(orderDtoList);

        return customerDto;
    }

    /**
     * Permet de convertir un CustomerDto en Customer
     * @param customerDto à convertir
     * @return un Customer
     */
    public static Customer toDomain(CustomerDto customerDto) {

        Customer customer = new Customer();

        customer.setId(customerDto.getId());
        customer.setFirstName(customerDto.getFirstName());
        customer.setLastName(customerDto.getLastName());
        customer.setUserName(customerDto.getUserName());
        if (customerDto.getBasketDto() != null ) {
            customer.setBasket(BasketDto.toDomain(customerDto.getBasketDto()));
        }


        List<Order> orders = new ArrayList<>();
        if(customerDto.getOrderDtos() != null) {
            for (OrderDto orderDto : customerDto.getOrderDtos()) {
                orders.add(OrderDto.toDomain(orderDto));
            }
        }

        customer.setOrders(orders);

        return customer;
    }
}
