package fr.zenika.zenika_emarket_v3.shop.controller;

import fr.zenika.zenika_emarket_v3.shop.controller.dto.CustomerDto;
import fr.zenika.zenika_emarket_v3.shop.domain.Customer;
import fr.zenika.zenika_emarket_v3.shop.filter.CustomerFilterDto;
import fr.zenika.zenika_emarket_v3.shop.service.CustomerService;
import fr.zenika.zenika_emarket_v3.utils.PageDto;
import lombok.Data;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.SortDefault;
import org.springframework.web.bind.annotation.*;
import static fr.zenika.zenika_emarket_v3.shop.filter.CustomerSpecifications.fromFilter;

/**
 * @author Emeline Hourmand
 */
@Data
@RestController
@CrossOrigin
@RequestMapping("/customers")
public class CustomerController {

    private final CustomerService customerService;

    /**
     * Permet de récupérer les customers
     * @param pageable les instructions pour retourner les customers
     * @return la liste de customers paginé
     */
    @GetMapping
    public PageDto<CustomerDto> getAllCustomer(@SortDefault(value = {"lastName"}) Pageable pageable,
                                               CustomerFilterDto filter) {
        Specification<Customer> customerSpecification = fromFilter(filter);
        return PageDto.fromDomain(
                customerService.getCustomers(pageable, customerSpecification), CustomerDto::fromDomain);
    }

    /**
     * Permet de récupérer un Customer par son attribue Id
     * @param id du Customer
     * @return un CustomerDto
     */
    @GetMapping("/{id}")
    public CustomerDto getCustomer(@PathVariable Integer id) {
        return CustomerDto.fromDomain(customerService.getCustomerById(id));
    }

    /**
     * Permet d'ajouter un nouveau Customer
     * @param customerDto le Customer à ajouter
     * @return un CustomerDto
     */
    @PostMapping
    public CustomerDto addCustomer(@RequestBody CustomerDto customerDto) {
        return CustomerDto.fromDomain(customerService.addCustomer(CustomerDto.toDomain(customerDto)));
    }

    /**
     * Permet de mettre à jour les données d'un Customer
     * @param id du Customer
     * @param customerDto les modifications du Customer
     * @return un CustomerDto
     */
    @PutMapping("/{id}")
    public CustomerDto updateCustomer(@PathVariable Integer id, @RequestBody CustomerDto customerDto) {
        return CustomerDto.fromDomain(customerService.updateCustomer(id, CustomerDto.toDomain(customerDto)));
    }

    /**
     * Permet de supprimer un Customer
     * @param id du Customer
     */
    @DeleteMapping("/{id}")
    public void deleteCustomer(@PathVariable Integer id) {
        customerService.deleteCustomer(id);
    }

}
