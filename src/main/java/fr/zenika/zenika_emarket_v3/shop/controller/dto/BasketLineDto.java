package fr.zenika.zenika_emarket_v3.shop.controller.dto;

import fr.zenika.zenika_emarket_v3.catalog.controller.dto.ProductDto;
import fr.zenika.zenika_emarket_v3.shop.domain.BasketLine;
import lombok.Data;

/**
 * @author Emeline Hourmand
 */
@Data
public class BasketLineDto {

    private ProductDto productDto;
    private Integer quantity;

    /**
     * Permet de convertir une BasketLine en BasketLineDto
     * @param line une BasketLine à convertir
     * @return une BasketLineDto
     */
    public static BasketLineDto fromDomain(BasketLine line) {

        BasketLineDto basketLineDto = new BasketLineDto();

        basketLineDto.setProductDto(ProductDto.fromDomain(line.getProduct()));
        basketLineDto.setQuantity(line.getQuantity());

        return basketLineDto;
    }

    /**
     * Permet de convertir une BasketLineDto en BasketLine
     * @param basketLineDto à convertir
     * @return une BasketLine
     */
    public static BasketLine toDomain(BasketLineDto basketLineDto) {

        BasketLine basketLine = new BasketLine();

        basketLine.setProduct(ProductDto.toDomain(basketLineDto.getProductDto()));
        basketLine.setQuantity(basketLineDto.getQuantity());

        return basketLine;
    }
}
