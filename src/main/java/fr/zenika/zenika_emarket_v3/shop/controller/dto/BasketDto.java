package fr.zenika.zenika_emarket_v3.shop.controller.dto;

import fr.zenika.zenika_emarket_v3.shop.domain.Basket;
import fr.zenika.zenika_emarket_v3.shop.domain.BasketLine;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Emeline Hourmand
 */
@Data
public class BasketDto {

    private int id;
    private LocalDateTime createdAt;
    private List<BasketLineDto> lines;

    /**
     * Permet de convertir un objet Basket en objet BasketDto
     * @param basket à convertir en objet BasketDto
     * @return un objet BasketDto
     */
    public static BasketDto fromDomain(Basket basket) {

        BasketDto basketDto = new BasketDto();

        basketDto.setId(basket.getId());
        basketDto.setCreatedAt(basket.getCreatedAt());

        List<BasketLineDto> basketLineDto  = new ArrayList<>();
        for (BasketLine line : basket.getBasketLines()) {
            basketLineDto.add(BasketLineDto.fromDomain(line));
        }

        basketDto.setLines(basketLineDto);

        return basketDto;
    }

    /**
     * Permet de convertir un objet BasketDto en objet Basket
     * @param basketDto à convertir en objet Basket
     * @return un objet Basket
     */
    public static Basket toDomain(BasketDto basketDto) {

        Basket basket = new Basket();

        basket.setId(basketDto.getId());
        basket.setCreatedAt(basketDto.getCreatedAt());

        List<BasketLine> basketLines = new ArrayList<>();
        if (basketDto.getLines() != null) {
            for (BasketLineDto basketLineDto : basketDto.getLines()) {
                basketLines.add(BasketLineDto.toDomain(basketLineDto));
            }
        }

        basket.setBasketLines(basketLines);

        return basket;
    }

}
