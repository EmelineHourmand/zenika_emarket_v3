package fr.zenika.zenika_emarket_v3.shop.controller.dto;

import fr.zenika.zenika_emarket_v3.shop.domain.Order;
import fr.zenika.zenika_emarket_v3.shop.domain.OrderLine;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Emeline Hourmand
 */
@Data
public class OrderDto {

    private Integer id;
    private LocalDateTime orderAt;
    private List<OrderLineDto> orderLines;
    private Double totalPriceTaxIncl;
    private Double totalPriceWtax;

    /**
     * Permet de convertir un Order en OrderDto
     * @param order à convertir
     * @return un OrderDto
     */
    public static OrderDto fromDomain(Order order) {
        OrderDto orderDto = new OrderDto();

        orderDto.setId(order.getId());
        orderDto.setOrderAt(order.getOrderAt());
        orderDto.setTotalPriceTaxIncl(order.getTotalPriceTaxIncl());
        orderDto.setTotalPriceWtax(order.getTotalPriceWtax());

        List<OrderLineDto> orderLineDto = new ArrayList<>();
        for (OrderLine line : order.getOrderLines()) {
            orderLineDto.add(OrderLineDto.fromDomain(line));
        }

        orderDto.setOrderLines(orderLineDto);

        return orderDto;
    }

    /**
     * Permet de convertir un OrderDto en Order
     * @param orderDto à convertir
     * @return un Order
     */
    public static Order toDomain(OrderDto orderDto) {
        Order order = new Order();

        order.setId(orderDto.getId());
        order.setOrderAt(orderDto.getOrderAt());
        order.setTotalPriceTaxIncl(orderDto.getTotalPriceTaxIncl());
        order.setTotalPriceWtax(orderDto.getTotalPriceWtax());

        List<OrderLine> orderLines = new ArrayList<>();
        for (OrderLineDto lines : orderDto.getOrderLines()) {
            orderLines.add(OrderLineDto.toDomain(lines));
        }

        order.setOrderLines(orderLines);

        return order;
    }


}
