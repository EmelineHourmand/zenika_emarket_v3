package fr.zenika.zenika_emarket_v3.shop.repository;

import fr.zenika.zenika_emarket_v3.shop.domain.Basket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Emeline Hourmand
 */
@Repository
public interface BasketRepository extends JpaRepository<Basket, Integer> { }