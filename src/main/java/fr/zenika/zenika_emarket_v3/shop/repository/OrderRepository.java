package fr.zenika.zenika_emarket_v3.shop.repository;

import fr.zenika.zenika_emarket_v3.shop.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Emeline Hourmand
 */
@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {
}
