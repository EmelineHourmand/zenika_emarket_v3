package fr.zenika.zenika_emarket_v3.shop.service;

import fr.zenika.zenika_emarket_v3.shop.domain.Order;
import fr.zenika.zenika_emarket_v3.shop.repository.OrderRepository;
import fr.zenika.zenika_emarket_v3.catalog.domain.Product;
import fr.zenika.zenika_emarket_v3.catalog.repository.ProductRepository;
import lombok.Data;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @author Emeline Hourmand
 */
@Data
@Service
public class OrderLineService {

    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;

    /**
     * Permet d'ajouter un Product dans une Order en BDD
     * @param idOrder Order
     * @param idProduct Product
     * @param quantity du Product
     * @return une Order
     */
    @Transactional
    public Order addProductToOrder(Integer idOrder, Integer idProduct, Integer quantity) {

        Order order = orderRepository.findById(idOrder)
                .orElseThrow(() -> new IllegalArgumentException("Order not found"));

        Product product = productRepository.findById(idProduct).
                orElseThrow(() -> new IllegalArgumentException("Product doesn't exist"));

        order.addProductToOrder(product, quantity);

        return order;
    }

}
