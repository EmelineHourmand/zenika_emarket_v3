package fr.zenika.zenika_emarket_v3.shop.service;

import fr.zenika.zenika_emarket_v3.shop.domain.Basket;
import fr.zenika.zenika_emarket_v3.shop.exception.BasketEmpty;
import fr.zenika.zenika_emarket_v3.shop.exception.BasketNotFound;
import fr.zenika.zenika_emarket_v3.shop.repository.BasketRepository;
import fr.zenika.zenika_emarket_v3.shop.domain.Customer;
import fr.zenika.zenika_emarket_v3.shop.exception.CustomerNotFound;
import fr.zenika.zenika_emarket_v3.shop.repository.CustomerRepository;
import fr.zenika.zenika_emarket_v3.shop.domain.Order;
import fr.zenika.zenika_emarket_v3.shop.exception.OrderNotFound;
import fr.zenika.zenika_emarket_v3.shop.repository.OrderRepository;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @author Emeline Hourmand
 */
@Data
@Service
public class OrderService {

    private final OrderRepository orderRepository;
    private final BasketRepository basketRepository;
    private final BasketLineService basketLineService;
    private final CustomerRepository customerRepository;

    /**
     * Permet de récupérer toutes les Order dans la BDD
     * @return une liste d'Order
     */
    @Transactional
    public Page<Order> getOrders(Pageable pageable) {
        return orderRepository.findAll(pageable);
    }

    /**
     * Permet de récupérer une Order par son Id dans la BDD
     * @param id de l'Order
     * @return une Order
     */
    @Transactional
    public Order getOrderById(Integer id) {
        return orderRepository.findById(id)
                .orElseThrow(() -> new OrderNotFound(id));
    }

    /**
     * Permet d'ajouter une Order en BDD et de vider le Basket du Customer
     * @param idBasket du Basket à vider
     * @param idCustomer Customer lié à l'Order
     * @return une Order
     */
    @Transactional
    public Order addOrder(Integer idBasket, Integer idCustomer) {
        Order order = new Order();

        Basket basket = basketRepository.findById(idBasket)
                .orElseThrow(() -> new BasketNotFound(idBasket));

        Customer customer = customerRepository.findById(idCustomer)
                .orElseThrow(() -> new CustomerNotFound(idCustomer));

        if (!basket.getBasketLines().isEmpty()) {
            order = order.setOrder(basket, customer);

            basketLineService.deleteAllProductIntoBasket(basket.getId());

            return orderRepository.save(order);
        } else {
            throw new BasketEmpty(basket.getId());
        }
    }

    /**
     * Permet de supprimer une Order de la BDD
     * @param id de l'Order
     */
    @Transactional
    public void deleteOrder(Integer id) {
        Order order = getOrderById(id);
        orderRepository.delete(order);
    }
}
