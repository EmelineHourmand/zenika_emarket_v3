package fr.zenika.zenika_emarket_v3.shop.service;

import fr.zenika.zenika_emarket_v3.shop.exception.BasketNotFound;
import fr.zenika.zenika_emarket_v3.shop.repository.BasketRepository;
import fr.zenika.zenika_emarket_v3.shop.domain.Basket;
import lombok.Data;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Emeline Hourmand
 */
@Data
@Service
public class BasketService {

    private final BasketRepository basketRepository;

    /**
     * Permet de récupérer une liste de Basket depuis la BDD
     * @return une liste de Basket
     */
    @Transactional
    public List<Basket> getBaskets() {
        return basketRepository.findAll();
    }

    /**
     * Permet de récupérer un Basket part son attribu Id depuis la BDD
     * @param id du Basket
     * @return un Basket
     */
    @Transactional
    public Basket getBasketById(Integer id) {
        return basketRepository.findById(id).
                orElseThrow(() -> new BasketNotFound(id));
    }

    /**
     * Permet d'ajouter un Basket dans la BDD
     * @param basket à ajouter dans la BDD
     * @return le Basket ajouté dans la BDD
     */
    @Transactional
    public Basket addBasket(Basket basket){

        if(basket.getCreatedAt() == null) {
            basket.setCreatedAt(LocalDateTime.now());
        }

        return basketRepository.save(basket);
    }

    /**
     * Permet de supprimer un Basket dans la BDD
     * @param id du Basket à supprimer
     */
    @Transactional
    public void deleteBasket(Integer id) {
        Basket basket = getBasketById(id);
        basketRepository.delete(basket);
    }

}
