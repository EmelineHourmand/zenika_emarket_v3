package fr.zenika.zenika_emarket_v3.shop.service;

import fr.zenika.zenika_emarket_v3.shop.exception.BasketNotFound;
import fr.zenika.zenika_emarket_v3.shop.repository.BasketRepository;
import fr.zenika.zenika_emarket_v3.shop.domain.Basket;
import fr.zenika.zenika_emarket_v3.catalog.domain.Product;
import fr.zenika.zenika_emarket_v3.catalog.exception.ProductNotFound;
import fr.zenika.zenika_emarket_v3.catalog.repository.ProductRepository;
import lombok.Data;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * @author Emeline Hourmand
 */
@Data
@Service
public class BasketLineService {

    private final BasketRepository basketRepository;
    private final ProductRepository productRepository;
    private final JdbcTemplate jdbcTemplate;

    public static final String SQL_DELETE_ALL_PRODUCT_INTO_BASKET =
            "DELETE FROM baskets_lines WHERE id_basket = ?";

    /**
     * Permet d'ajouter un Product dans un Basket en BDD
     * @param idBasket du Basket
     * @param idProduct du Product
     * @param quantity quantité de Product
     * @return un Basket
     */
    @Transactional
    public Basket addProductToBasket(Integer idBasket, Integer idProduct, Integer quantity) {

        Basket basket = basketRepository.findById(idBasket).
                orElseThrow(() -> new BasketNotFound(idBasket));

        Product product = productRepository.findById(idProduct).
                orElseThrow(() -> new ProductNotFound(idProduct));

        basket.addProductToBasket(product, quantity);
        basket.setCreatedAt(LocalDateTime.now());

        return basket;
    }

    /**
     * Permet de supprimer un Product dans un Basket
     * @param idBasket du Basket
     * @param idProduct du Product
     */
    @Transactional
    public void deleteProductIntoBasket(Integer idBasket, Integer idProduct) {

        Basket basket = basketRepository.findById(idBasket).

                orElseThrow(() -> new BasketNotFound(idBasket));

        Product product = productRepository.findById(idProduct).
                orElseThrow(() -> new ProductNotFound(idProduct));

        basket.getBasketLines().removeIf(basketLine -> Objects.equals(basketLine.getProduct(), product));

    }

    /**
     * Permet de supprimer toutes les BasketLines d'un Basket
     * @param idBasket des BasketLines
     */
    @Transactional
    public void deleteAllProductIntoBasket(Integer idBasket) {
        jdbcTemplate.update(SQL_DELETE_ALL_PRODUCT_INTO_BASKET, idBasket);
    }

}
