package fr.zenika.zenika_emarket_v3.shop.service;

import fr.zenika.zenika_emarket_v3.shop.domain.Basket;
import fr.zenika.zenika_emarket_v3.shop.exception.CustomerNotFound;
import fr.zenika.zenika_emarket_v3.shop.repository.CustomerRepository;
import fr.zenika.zenika_emarket_v3.shop.domain.Customer;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @author Emeline Hourmand
 */
@Data
@Service
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final BasketService basketService;

    /**
     * Permet de récupérer la liste des Customer depuis la BDD
     * @return une liste de Customer
     */
    @Transactional
    public Page<Customer> getCustomers(Pageable pageable, Specification<Customer> specification) {
        return customerRepository.findAll(specification, pageable);
    }

    /**
     * Permet de récupérer un Customer par son Id depuis la BDD
     * @param id du Customer
     * @return un Customer
     */
    @Transactional
    public Customer getCustomerById(Integer id) {
        return customerRepository.findById(id).
                orElseThrow(() -> new CustomerNotFound(id));
    }

    /**
     * Ajouter un Customer dans la BDD
     * @param customer à ajouter dans la BDD
     * @return le Customer ajouté dans la BDD
     */
    @Transactional
    public Customer addCustomer(Customer customer) {
        customer.setBasket(basketService.addBasket(new Basket()));
        return customerRepository.save(customer);
    }

    /**
     * Permet de mettre à jour un Customer dans la BDD
     * @param idCustomer du Customer
     * @param updatedCustomer les nouvelles informations du Customer
     * @return le Customer modifié
     */
    @Transactional
    public Customer updateCustomer(Integer idCustomer, Customer updatedCustomer) {

        Customer customer = getCustomerById(idCustomer);

        if (updatedCustomer.getFirstName() != null) {
            customer.setFirstName(updatedCustomer.getFirstName());
        }

        if (updatedCustomer.getLastName() != null) {
            customer.setLastName(updatedCustomer.getLastName());
        }

        if (updatedCustomer.getUserName() != null) {
            customer.setUserName(updatedCustomer.getUserName());
        }

        return customer;
    }

    /**
     * Permet de supprimer un utilisateur dans la BDD
     * @param idCustomer du Customer
     */
    @Transactional
    public void deleteCustomer(Integer idCustomer) {
        Customer customer = getCustomerById(idCustomer);
        basketService.deleteBasket(customer.getBasket().getId());
        customerRepository.delete(getCustomerById(customer.getId()));
    }
}
