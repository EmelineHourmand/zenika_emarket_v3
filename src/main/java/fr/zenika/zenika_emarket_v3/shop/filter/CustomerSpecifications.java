package fr.zenika.zenika_emarket_v3.shop.filter;

import fr.zenika.zenika_emarket_v3.shop.domain.Customer;
import org.springframework.data.jpa.domain.Specification;

import static fr.zenika.zenika_emarket_v3.utils.SpecificationUtils.hasAttributeEquals;
import static fr.zenika.zenika_emarket_v3.utils.SpecificationUtils.hasAttributeLike;

/**
 * @author Emeline Hourmand
 */
public class CustomerSpecifications {

    public static Specification<Customer> fromFilter(CustomerFilterDto filter) {
        return hasFirstName(filter.getFirstName())
                .and(hasFristNameLike(filter.getFirstNameLike()))
                .and(hastLastName(filter.getLastName()))
                .and(hasLastNameLike(filter.getLastNameLike()))
                .and(hasUserName(filter.getUserName()))
                .and(hasUserNameLike(filter.getUserNameLike()));
    }

    public static Specification<Customer> hasFirstName(String firstName) {
        return hasAttributeEquals("firstName", firstName);
    }

    public static Specification<Customer> hasFristNameLike(String firstName) {
        return hasAttributeLike("firstName", firstName);
    }

    public static Specification<Customer> hastLastName(String lastName) {
        return hasAttributeEquals("lastName", lastName);
    }

    public static Specification<Customer> hasLastNameLike(String lastName) {
        return hasAttributeLike("lastName", lastName);
    }

    public static Specification<Customer> hasUserName(String userName) {
        return hasAttributeEquals("userName", userName);
    }

    public static Specification<Customer> hasUserNameLike(String userName) {
        return hasAttributeLike("userName", userName);
    }

}
