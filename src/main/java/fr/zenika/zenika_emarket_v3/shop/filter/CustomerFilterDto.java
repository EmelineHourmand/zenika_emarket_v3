package fr.zenika.zenika_emarket_v3.shop.filter;

import lombok.Data;

/**
 * @author Emeline Hourmand
 */
@Data
public class CustomerFilterDto {

    private String firstName;
    private String firstNameLike;
    private String lastName;
    private String lastNameLike;
    private String userName;
    private String userNameLike;

}
