package fr.zenika.zenika_emarket_v3.shop.exception;

import fr.zenika.zenika_emarket_v3.exception.NotFoundException;

/**
 * @author Emeline Hourmand
 */
public class CustomerNotFound extends NotFoundException {

    /**
     * Information sur le CustomerByIdNotFound
     * @param id du CustomerNotFound
     */
    public CustomerNotFound(Integer id) {
        super(id, "Customer");
    }
}
