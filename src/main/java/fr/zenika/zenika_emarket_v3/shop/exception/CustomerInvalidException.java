package fr.zenika.zenika_emarket_v3.shop.exception;

import fr.zenika.zenika_emarket_v3.exception.InvalidException;

/**
 * @author Emeline Hourmand
 */
public class CustomerInvalidException extends InvalidException {

    public CustomerInvalidException(String attributeName) {
        super("Customer", attributeName);
    }
}
