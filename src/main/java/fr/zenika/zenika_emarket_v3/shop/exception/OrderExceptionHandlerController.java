package fr.zenika.zenika_emarket_v3.shop.exception;

import fr.zenika.zenika_emarket_v3.exception.ErrorHttp;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author Emeline Hourmand
 */
@ControllerAdvice
public class OrderExceptionHandlerController {

    /**
     * Permet de gérer l'exception lorsqu'une Order n'existe pas.
     * @param e un IdNotFoundException
     * @return une Err 404
     */
    @ExceptionHandler(OrderNotFound.class)
    public ResponseEntity<ErrorHttp> onIdNotFoundException(OrderNotFound e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                new ErrorHttp(e.getTimestamp() + " : " +
                        e.getEntityName() + " with id " +
                        e.getId() + " not found"));
    }
}
