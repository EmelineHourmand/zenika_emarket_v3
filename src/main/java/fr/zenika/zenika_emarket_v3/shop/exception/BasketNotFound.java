package fr.zenika.zenika_emarket_v3.shop.exception;

import fr.zenika.zenika_emarket_v3.exception.NotFoundException;

/**
 * @author Emeline Hourmand
 */
public class BasketNotFound extends NotFoundException {

    /**
     * Information sur le BasketNotFound
     * @param id du BasketNotFound
     */
    public BasketNotFound(Integer id) {
        super(id, "Basket");
    }

}