package fr.zenika.zenika_emarket_v3.shop.exception;

import fr.zenika.zenika_emarket_v3.exception.FunctionalException;

public class BasketEmpty extends FunctionalException {

    private Integer id;
    private String entityName;

    public BasketEmpty(Integer id) {
        this.id = id;
        this.entityName = "Basket";
    }

    public Integer getId() {
        return id;
    }

    public String getEntityName() {
        return entityName;
    }
}
