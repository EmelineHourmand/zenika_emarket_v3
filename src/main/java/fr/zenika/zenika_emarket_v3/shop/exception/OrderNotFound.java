package fr.zenika.zenika_emarket_v3.shop.exception;

import fr.zenika.zenika_emarket_v3.exception.NotFoundException;

/**
 * @author Emeline Hourmand
 */
public class OrderNotFound extends NotFoundException {

    /**
     * Information sur le OrderByIdNotFound
     * @param id du OrderNotFound
     */
    public OrderNotFound(Integer id) {
        super(id, "Order");
    }
}
