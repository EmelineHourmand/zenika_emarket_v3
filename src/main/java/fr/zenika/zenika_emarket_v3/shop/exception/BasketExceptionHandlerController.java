package fr.zenika.zenika_emarket_v3.shop.exception;

import fr.zenika.zenika_emarket_v3.exception.ErrorHttp;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author Emeline Hourmand
 */
@ControllerAdvice
public class BasketExceptionHandlerController {

    /**
     * Permet de gérer l'exception lorsqu'un Basket n'existe pas.
     * @param e un NotFoundException
     * @return une Err 404
     */
    @ExceptionHandler(BasketNotFound.class)
    public ResponseEntity<ErrorHttp> onIdNotFoundException(BasketNotFound e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                new ErrorHttp(e.getTimestamp() + " : " +
                        e.getEntityName() + " with id " +
                        e.getId() + " not found"));
    }

    /**
     * Permet de gérer l'exception lorsqu'un Basket est vide.
     * @param e un EmptyException
     * @return une Err 400
     */
    @ExceptionHandler(BasketEmpty.class)
    public ResponseEntity<ErrorHttp> onIsEmptyException(BasketEmpty e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                new ErrorHttp(e.getTimestamp() + " : " +
                        e.getEntityName() + " " +
                        e.getId() + " is empty"));
    }
}
