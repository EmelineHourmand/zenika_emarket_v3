package fr.zenika.zenika_emarket_v3.shop.domain;

import fr.zenika.zenika_emarket_v3.catalog.domain.Product;
import fr.zenika.zenika_emarket_v3.shop.domain.BasketLine;
import fr.zenika.zenika_emarket_v3.shop.domain.Customer;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Emeline Hourmand
 */
@Entity
@Table(name = "baskets")
public class Basket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_basket")
    private Integer id;

    @OneToOne(mappedBy = "basket")
    private Customer customer;

    @ElementCollection
    @CollectionTable(name = "baskets_lines",
            joinColumns = @JoinColumn(name = "id_basket", referencedColumnName = "id_basket"))
    private List<BasketLine> basketLines = new ArrayList<>();
    private LocalDateTime createdAt;

    public Basket() {
        this.setCreatedAt(LocalDateTime.now());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public List<BasketLine> getBasketLines() {
        return basketLines;
    }

    public void setBasketLines(List<BasketLine> basketLines) {
        this.basketLines = basketLines;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    /**
     * Permet d'ajouter un Product dans un BasketLine
     * @param product product à ajouter
     * @param quantity quantité du product à ajouter
     * @return un BasketLine
     */
    public BasketLine addProductToBasket(Product product, Integer quantity) {

        BasketLine basketLine = getBasketLineFromProduct(product);

        if( basketLine == null ) {
            basketLine = new BasketLine(product, quantity);
            basketLines.add(basketLine);
        } else {
            basketLine.setQuantity(basketLine.getQuantity() + quantity);
        }

        return basketLine;
    }

    /**
     * Permet de récupérer le Product d'un BasketLine
     * @param product product à récupérer
     * @return un BasketLine
     */
    public BasketLine getBasketLineFromProduct(Product product) {

        for (BasketLine basketLine : basketLines) {
            if (Objects.equals(basketLine.getProduct().getId(), product.getId())) {
                return basketLine;
            }
        }

        return null;
    }
}
