package fr.zenika.zenika_emarket_v3.shop.domain;

import fr.zenika.zenika_emarket_v3.shop.exception.CustomerInvalidException;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Emeline Hourmand
 */
@Entity
@Table(name = "customers")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_customer")
    private Integer id;
    private String firstName;
    private String lastName;
    private String userName;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_basket", referencedColumnName = "id_basket")
    private Basket basket;

    @OneToMany(mappedBy = "customer")
    private List<Order> orders = new ArrayList<>();

    public Customer() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if(!StringUtils.hasText(firstName)) {
            throw new CustomerInvalidException("firstName");
        }
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        if(!StringUtils.hasText(lastName)){
            throw new CustomerInvalidException("lastName");
        }
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        if(!StringUtils.hasText(userName)){
            throw new CustomerInvalidException(userName);
        }
        this.userName = userName;
    }

    public Basket getBasket() {
        return basket;
    }

    public void setBasket(Basket basket) {
        this.basket = basket;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
