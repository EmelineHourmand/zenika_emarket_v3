package fr.zenika.zenika_emarket_v3.shop.domain;

import fr.zenika.zenika_emarket_v3.catalog.domain.Product;

import javax.persistence.*;

/**
 * @author Emeline Hourmand
 */
@Embeddable
public class BasketLine {

    @ManyToOne
    @JoinColumn(name = "id_product", referencedColumnName = "id_product")
    private Product product;
    private Integer quantity;

    public BasketLine() {}

    public BasketLine(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
