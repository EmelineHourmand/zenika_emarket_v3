package fr.zenika.zenika_emarket_v3.shop.domain;

import fr.zenika.zenika_emarket_v3.catalog.domain.Product;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * @author Emeline Hourmand
 */
@Embeddable
public class OrderLine {

    @ManyToOne
    @JoinColumn(name = "id_product", referencedColumnName = "id_product")
    private Product products;
    private Integer quantity;
    private Double priceTaxInlc;
    private Double priceWtax;

    public OrderLine() {}

    public OrderLine(Product products, Integer quantity, Double priceTaxInlc, Double priceWtax) {
        this.products = products;
        this.quantity = quantity;
        this.priceTaxInlc = priceTaxInlc;
        this.priceWtax = priceWtax;
    }

    public Product getProducts() {
        return products;
    }

    public void setProducts(Product products) {
        this.products = products;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getPriceTaxInlc() {
        return priceTaxInlc;
    }

    public void setPriceTaxInlc(Double priceTaxInlc) {
        this.priceTaxInlc = priceTaxInlc;
    }

    public Double getPriceWtax() {
        return priceWtax;
    }

    public void setPriceWtax(Double priceWtax) {
        this.priceWtax = priceWtax;
    }
}
