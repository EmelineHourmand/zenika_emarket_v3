package fr.zenika.zenika_emarket_v3.shop.domain;

import fr.zenika.zenika_emarket_v3.catalog.domain.Product;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Emeline Hourmand
 */
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_order")
    private Integer id;

    private LocalDateTime orderAt;

    @ElementCollection
    @CollectionTable(name = "orders_lines",
            joinColumns = @JoinColumn(name = "id_order", referencedColumnName = "id_order"))
    private List<OrderLine> orderLines = new ArrayList<>();

    private Double totalPriceTaxIncl;
    private Double totalPriceWtax;

    @ManyToOne
    @JoinColumn(name = "id_customer", nullable = false)
    private Customer customer;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getOrderAt() {
        return orderAt;
    }

    public void setOrderAt(LocalDateTime orderAt) {
        this.orderAt = orderAt;
    }

    public List<OrderLine> getOrderLines() {
        return orderLines;
    }

    public void setOrderLines(List<OrderLine> orderLines) {
        this.orderLines = orderLines;
    }

    public Double getTotalPriceTaxIncl() {
        return totalPriceTaxIncl;
    }

    public void setTotalPriceTaxIncl(Double totalPriceTaxIncl) {
        this.totalPriceTaxIncl = totalPriceTaxIncl;
    }

    public Double getTotalPriceWtax() {
        return totalPriceWtax;
    }

    public void setTotalPriceWtax(Double totalPriceWtax) {
        this.totalPriceWtax = totalPriceWtax;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    /**
     * Permet d'ajouter un Product dans une Order
     *
     * @param product  à ajouter
     * @param quantity du Product
     * @return Une OrderLine
     */
    public OrderLine addProductToOrder(Product product, Integer quantity) {
        OrderLine orderLine = getOrderLineFromProduct(product);

        if (orderLine == null) {
            orderLine = new OrderLine(product, quantity, product.getTaxInclPrice(), product.getWtPrice());
            orderLines.add(orderLine);
        } else {
            orderLine.setQuantity(orderLine.getQuantity() + quantity);
        }

        return orderLine;
    }

    /**
     * Permet de récupérer les Product dans une OrderLine
     *
     * @param product à récupérer
     * @return une OrderLine ou null
     */
    private OrderLine getOrderLineFromProduct(Product product) {
        for (OrderLine orderLine : orderLines) {
            if (Objects.equals(orderLine.getProducts().getId(), product.getId())) {
                return orderLine;
            }
        }

        return null;
    }

    /**
     * Peremt de convertir un Basket en Order
     * @param basket à convertir
     * @param customer de l'Order
     */
    public Order setOrder(Basket basket, Customer customer) {

        Order order = new Order();

        order.setOrderAt(LocalDateTime.now());
        order.setCustomer(customer);
        order.setOrderLines(transformBasketLineToOrderLine(basket));
        order.calculateTotalPriceIncl(order);

        return order;
    }

    /**
     * Permet de calculer le totalPrice pour l'Oder en entier
     * @param order l'order concerné
     */
    private void calculateTotalPriceIncl(Order order) {
        Double totalPriceTaxIncl = 0.0;
        Double totalPriceWTax = 0.0;
        for (OrderLine orderLine : order.getOrderLines()) {
            totalPriceWTax += orderLine.getProducts().getWtPrice() * orderLine.getQuantity();
            totalPriceTaxIncl += orderLine.getProducts().getTaxInclPrice() * orderLine.getQuantity();
        }
        order.setTotalPriceTaxIncl(totalPriceTaxIncl);
        order.setTotalPriceWtax(totalPriceWTax);

    }

    /**
     * Permet de transférer les données d'un Basket pour créer une Liste d'OrderLine
     * @param basket à transférer
     * @return une list d'orderLine
     */
    private List<OrderLine> transformBasketLineToOrderLine(Basket basket) {

        List<OrderLine> orderLines = new ArrayList<>();
        for (BasketLine basketLine : basket.getBasketLines()) {
            OrderLine orderLine = new OrderLine();
            orderLine.setProducts(basketLine.getProduct());
            orderLine.setQuantity(basketLine.getQuantity());
            orderLine.setPriceTaxInlc(basketLine.getProduct().getTaxInclPrice());
            orderLine.setPriceWtax(basketLine.getProduct().getWtPrice());
            orderLines.add(orderLine);
        }

        return orderLines;
    }
}
