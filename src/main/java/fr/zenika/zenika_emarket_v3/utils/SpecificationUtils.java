package fr.zenika.zenika_emarket_v3.utils;

import org.springframework.data.jpa.domain.Specification;

/**
 * @author Emeline Hourmand
 */
public class SpecificationUtils {

    /**
     * Permet de récupérer un objet égal au champ passé en param
     * @param attribute Nom de l'attribue recherché
     * @param value valeur de l'attribue recherché
     * @param <V> valeur recherchée
     * @param <E> classe ou la valeur est recherché
     * @return null si valeur non trouvé, sinon retourne la valeur trouvé
     */
    public static <V, E> Specification<E> hasAttributeEquals(String attribute, V value) {
        if (value == null) {
            return ((root, query, criteriaBuilder) -> null);
        } else {
            return ((root, query, criteriaBuilder) ->
                    criteriaBuilder.equal(root.get(attribute), value.toString()));
        }
    }

    /**
     * Permet de récupérer des objets ayant un champ ressemblant au champ passé en param
     * @param attribute recherché
     * @param value valeur de l'attribut recherché
     * @param <V> valeur recherchée
     * @param <E> classe ou la valeur est recherché
     * @return null si non trouvé, sinon retourne les valeurs trouvées
     */
    public static <V, E> Specification<E> hasAttributeLike(String attribute, String value) {
        if (value ==null) {
            return ((root, query, criteriaBuilder) -> null);
        } else {
            return ((root, query, criteriaBuilder) ->
                    criteriaBuilder.like(criteriaBuilder.lower(root.get(attribute)), value.toLowerCase() + "%"));
        }
    }

    /**
     * Permet de récupérer les objets ayant une valeur supérieure ou égal au champ passé en param
     * @param attribute recherché
     * @param value de l'attribut recherché
     * @param <V> valeur de l'attribut recherché
     * @param <E> classe ou la valeur est recherché
     * @return null si non trouvé, sinon retourne les valeurs trouvées
     */
    public static <V extends Comparable<? super V>, E> Specification<E>
    hasAttributeGreaterThanOrEqualsTo(String attribute, V value) {
        if (value == null) {
            return ((root, query, criteriaBuilder) -> null );
        } else {
            return ((root, query, criteriaBuilder) ->
                    criteriaBuilder.greaterThanOrEqualTo(root.get(attribute), value));
        }
    }

    /**
     * Permet de récupérer les objets ayant une valeur inférieure au champ passé en param
     * @param attribute recherché
     * @param value de l'attribut recherché
     * @param <V> valeur ed l'attribut recherché
     * @param <E> classe ou la valeur est recherché
     * @return null si non trouvé, sinon retourne les valeurs trouvées
     */
    public static <V extends Comparable<? super V>, E> Specification<E>
    hasAttributeLessThan(String attribute, V value) {
        if (value == null) {
            return ((root, query, criteriaBuilder) -> null);
        } else {
            return ((root, query, criteriaBuilder) -> criteriaBuilder.lessThan(root.get(attribute), value));
        }
    }
}
