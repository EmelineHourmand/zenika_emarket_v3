create table IF NOT EXISTS categories
(
    id_category integer generated by default as identity
        constraint category_pk
            primary key,
    name        text not null
);

create index categories_name_index
    on categories (name);

create table IF NOT EXISTS products
(
    id_product     integer generated by default as identity
        constraint products_pk
            primary key,
    name           text    not null,
    tax_incl_price numeric not null,
    wt_price       numeric not null,
    description    text,
    id_category    integer default 1
        constraint products_categories_id_category_fk
            references categories
);

create index products_name_index
    on products (name);

create table IF NOT EXISTS baskets
(
    id_basket  integer generated by default as identity
        constraint baskets_pk
            primary key,
    created_at timestamp default now()
);

create table IF NOT EXISTS customers
(
    id_customer integer generated by default as identity
        constraint customers_pk
            primary key,
    first_name  text not null,
    last_name   text,
    user_name   text,
    id_basket   integer
        constraint customers_baskets_id_basket_fk
            references baskets
);

create index customers_first_name_index
    on customers (first_name);

create index customers_last_name_index
    on customers (last_name);

create index customers_user_name_index
    on customers (user_name);

create table IF NOT EXISTS baskets_lines
(
    id_basket  integer           not null
        constraint baskets_lines_baskets_id_basket_fk
            references baskets,
    id_product integer           not null
        constraint baskets_lines_products_id_product_fk
            references products,
    quantity   integer default 1 not null
);


create table IF NOT EXISTS orders
(
    id_order integer generated by default as identity
        constraint order_pk
            primary key,
    order_at             timestamp                                               not null,
    total_price_tax_incl numeric,
    total_price_wtax     numeric,
    id_customer          integer                                                 not null
        constraint orders_customers_id_customer_fk
            references customers
);

create unique index order_id_order_uindex
    on orders (id_order);

create table IF NOT EXISTS orders_lines
(
    id_order       integer not null
        constraint orders_lines_order_id_order_fk
            references orders,
    id_product     integer not null
        constraint orders_lines_products_id_product_fk
            references products,
    quantity       integer not null,
    price_tax_inlc numeric,
    price_wtax     numeric
);