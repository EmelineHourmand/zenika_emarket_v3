INSERT INTO baskets (created_at)
VALUES
    (now()),
    (now());

INSERT INTO customers (first_name, last_name, user_name, id_basket)
VALUES
    ('Jefferson', 'Turner', 'Mino', 1),
    ('Paco', 'Saez', 'Delta', 2);

INSERT INTO categories (name)
VALUES
    ('Default'),
    ('Fruits');

INSERT INTO products (name, tax_incl_price, wt_price, description, id_category)
VALUES
    ('Fraises', 3.5, 2.95, 'Une barquette de fraises.', 2),
    ('Pommes', 2.5, 1.95, 'Un sachet de pommes.', 2),
    ('Poires', 3.75, 3, 'Un sachet de poires.', 2),
    ('Bananes', 1.5, 0.85, 'Des bananes', 2);

INSERT INTO baskets_lines (id_basket, id_product, quantity)
VALUES (1, 2, 25),
       (1, 4, 5),
       (2, 1, 12),
       (2, 3, 6);