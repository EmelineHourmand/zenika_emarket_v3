-- Drop all data into these tables

delete from baskets_lines;
delete from orders_lines;
delete from customers;
delete from baskets;
delete from orders;
delete from products;
delete from categories;
