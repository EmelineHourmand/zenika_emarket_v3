package fr.zenika.zenika_emarket_v3.shop;

import fr.zenika.zenika_emarket_v3.shop.factory.CustomerFactory;
import fr.zenika.zenika_emarket_v3.shop.domain.Customer;
import fr.zenika.zenika_emarket_v3.shop.exception.CustomerInvalidException;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * @author Emeline Hourmand
 */
class CustomerTest {

    @Test
    void should_create_customer() {
        Customer customer = CustomerFactory.jefferson();
        assertThat(customer).isNotNull();
    }

    @Test
    void should_reject_empty_firstName() {
        assertThatThrownBy(() -> new Customer().setFirstName(""))
                .isInstanceOf(CustomerInvalidException.class);
    }

    @Test
    void should_reject_empty_lastName() {
        assertThatThrownBy(() -> new Customer().setLastName(""))
                .isInstanceOf(CustomerInvalidException.class);
    }

    @Test
    void should_reject_empty_userName() {
        assertThatThrownBy(() -> new Customer().setUserName(""))
                .isInstanceOf(CustomerInvalidException.class);
    }
}