package fr.zenika.zenika_emarket_v3.shop;

import fr.zenika.zenika_emarket_v3.shop.domain.Basket;
import fr.zenika.zenika_emarket_v3.catalog.factory.CategoryFactory;
import fr.zenika.zenika_emarket_v3.catalog.factory.ProductFactory;
import fr.zenika.zenika_emarket_v3.shop.factory.BasketFactory;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;
/**
 * @author Emeline Hourmand
 */
public class BasketTest {

    @Test
    void should_create_basket(){
        Basket basket = BasketFactory.basketJefferson();
        assertThat(basket).isNotNull();
    }

    @Test
    void should_have_customer(){
        Basket basket = BasketFactory.basketPaco();
        assertThat(basket.getCustomer()).isNotNull();
    }

    @Test
    void should_have_create_at_date(){
        Basket basket = BasketFactory.basketJefferson();
        assertThat(basket.getCreatedAt()).isNotNull();
    }

    @Test
    void should_have_empty_basket(){
        Basket basket = BasketFactory.basketPaco();
        assertThat(basket.getBasketLines().size()).isEqualTo(0);
    }

    @Test
    void should_have_product_in_basket(){
        Basket basket = BasketFactory.basketJefferson();
        assertThat(basket.getBasketLines().size()).isEqualTo(2);
    }

    @Test
    void should_add_product_to_basket(){
        Basket basket = BasketFactory.basketPaco();
        assertThat(basket.getBasketLines().size()).isEqualTo(0);
        basket.addProductToBasket(ProductFactory.gta5(CategoryFactory.defaultCategory()), 1);
        assertThat(basket.getBasketLines().size()).isEqualTo(1);
    }

    @Test
    void should_get_product_to_basket(){

    }

    @Test
    void should_get_new_product_to_basket(){

    }
}
