package fr.zenika.zenika_emarket_v3.shop;

import fr.zenika.zenika_emarket_v3.shop.factory.CustomerFactory;
import fr.zenika.zenika_emarket_v3.shop.controller.dto.BasketDto;
import fr.zenika.zenika_emarket_v3.shop.controller.dto.CustomerDto;
import fr.zenika.zenika_emarket_v3.shop.domain.Customer;
import fr.zenika.zenika_emarket_v3.exception.ErrorHttp;
import fr.zenika.zenika_emarket_v3.it.EntitesPersister;
import fr.zenika.zenika_emarket_v3.it.IntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static org.assertj.core.api.Assertions.*;

/**
 * @author Emeline Hourmand
 */
@IntegrationTest
public class CustomerIt {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private EntitesPersister entitesPersister;

    @Test
    void should_return_not_found_on_unknown_customer() {
        ResponseEntity<ErrorHttp> response =
                restTemplate.getForEntity("/customers/{id}",
                        ErrorHttp.class, 5);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getMessage()).isNotNull();
    }

    @Test
    void should_return_existing_customer() {
        Customer customer = CustomerFactory.jefferson();
        entitesPersister.persist(customer);

        ResponseEntity<CustomerDto> response = restTemplate.getForEntity("/customers/{id}",
                CustomerDto.class, customer.getId());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull()
                .returns(customer.getId(), CustomerDto::getId)
                .returns(customer.getFirstName(), CustomerDto::getFirstName)
                .returns(customer.getLastName(), CustomerDto::getLastName)
                .returns(customer.getUserName(), CustomerDto::getUserName);

        assertThat(response.getBody().getBasketDto()).isNotNull()
                .returns(customer.getBasket().getId(), BasketDto::getId);
        assertThat(response.getBody().getBasketDto().getCreatedAt()).isCloseTo(LocalDateTime.now(), within(2, ChronoUnit.SECONDS));
        assertThat(response.getBody().getOrderDtos().size()).isNotNull();

    }

    @Test
    void should_delete_existing_customer() {
        Customer customer = CustomerFactory.paco();
        entitesPersister.persist(customer);

        ResponseEntity<Void> deleteResponse = restTemplate
                .exchange(URI.create("/customers/" + customer.getId()),
                        HttpMethod.DELETE, null, Void.class);

        assertThat(deleteResponse.getStatusCode()).isEqualTo(HttpStatus.OK);

        ResponseEntity<ErrorHttp> response = restTemplate
                .getForEntity("/customer/{id}", ErrorHttp.class, customer.getId());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void should_create_new_customer() {
        CustomerDto customerDto = new CustomerDto();
        customerDto.setFirstName("Manolo");
        customerDto.setLastName("Lombard");
        customerDto.setUserName("Alpha");
        customerDto.setBasketDto(new BasketDto());

        ResponseEntity<CustomerDto> createResponse = restTemplate
                .postForEntity("/customers", customerDto, CustomerDto.class);

        assertThat(createResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(createResponse.getBody().getId()).isNotNull();
    }

    @Test
    void should_update_customer() {
        Customer jefferson = CustomerFactory.jefferson();
        entitesPersister.persist(jefferson);

        CustomerDto customerRequest = new CustomerDto()
                .setUserName("Mino");

        restTemplate
                .put("/customers/{id}", customerRequest, jefferson.getId());

        ResponseEntity<CustomerDto> getResponse = restTemplate.getForEntity(
                URI.create("/customers/" + jefferson.getId()),
                CustomerDto.class);

        assertThat(getResponse.getBody())
                .returns("Mino", CustomerDto::getUserName);
    }


}
