package fr.zenika.zenika_emarket_v3.shop.factory;

import fr.zenika.zenika_emarket_v3.shop.domain.Basket;
import fr.zenika.zenika_emarket_v3.shop.domain.BasketLine;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Emeline Hourmand
 */
public class BasketFactory {

    public static Basket basketJefferson() {
        Basket basketJefferson = new Basket();
        basketJefferson.setCreatedAt(LocalDateTime.now());
        basketJefferson.setCustomer(CustomerFactory.jefferson());

        List<BasketLine> lines = new ArrayList<>();
        lines.add(BasketLineFactory.basketLineBanane());
        lines.add(BasketLineFactory.basketLineGta5());

        basketJefferson.setBasketLines(lines);

        return basketJefferson;
    }

    public static Basket basketPaco() {
        Basket basketPaco = new Basket();
        basketPaco.setCreatedAt(LocalDateTime.now());
        basketPaco.setCustomer(CustomerFactory.paco());

        List<BasketLine> lines = new ArrayList<>();
        basketPaco.setBasketLines(lines);

        return basketPaco;
    }
}
