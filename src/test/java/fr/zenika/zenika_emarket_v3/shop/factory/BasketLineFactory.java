package fr.zenika.zenika_emarket_v3.shop.factory;

import fr.zenika.zenika_emarket_v3.shop.domain.BasketLine;
import fr.zenika.zenika_emarket_v3.catalog.factory.CategoryFactory;
import fr.zenika.zenika_emarket_v3.catalog.factory.ProductFactory;

/**
 * @author Emeline Hourmand
 */
public class BasketLineFactory {

    public static BasketLine basketLineBanane() {
        BasketLine basketLineBanane = new BasketLine();
        basketLineBanane.setProduct(ProductFactory.banane(CategoryFactory.fruits()));
        basketLineBanane.setQuantity(5);

        return basketLineBanane;
    }

    public static BasketLine basketLineGta5() {
        BasketLine basketLineGta5 = new BasketLine();
        basketLineGta5.setProduct(ProductFactory.gta5(CategoryFactory.defaultCategory()));
        basketLineGta5.setQuantity(1);

        return basketLineGta5;
    }
}
