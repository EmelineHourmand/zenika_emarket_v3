package fr.zenika.zenika_emarket_v3.shop.factory;

import fr.zenika.zenika_emarket_v3.shop.domain.Basket;
import fr.zenika.zenika_emarket_v3.shop.domain.Customer;

/**
 * @author Emeline Hourmand
 */
public class CustomerFactory {

    public static Customer jefferson(){
        Customer customer = new Customer();
        customer.setFirstName("Jefferson");
        customer.setLastName("Turner");
        customer.setUserName("Mino");
        customer.setBasket(new Basket());
        return customer;
    }

    public static Customer paco() {
        Customer customer = new Customer();
        customer.setFirstName("Paco");
        customer.setLastName("Saez");
        customer.setUserName("Kimbo");
        customer.setBasket(new Basket());
        return customer;
    }
}
