package fr.zenika.zenika_emarket_v3.shop;

import fr.zenika.zenika_emarket_v3.shop.factory.BasketLineFactory;
import fr.zenika.zenika_emarket_v3.shop.domain.BasketLine;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

/**
 * @author Emeline Hourmand
 */
public class BasketLineTest {

    @Test
    void should_create_basket_line() {
        BasketLine basketLine = BasketLineFactory.basketLineBanane();
        assertThat(basketLine).isNotNull();
    }

    @Test
    void should_have_quantity_better_than_0(){
        BasketLine basketLine = BasketLineFactory.basketLineGta5();
        assert(basketLine.getQuantity() > 0);
    }

    @Test
    void should_have_product() {
        BasketLine basketLine = BasketLineFactory.basketLineBanane();
        assertThat(basketLine.getProduct()).isNotNull();
    }

}
