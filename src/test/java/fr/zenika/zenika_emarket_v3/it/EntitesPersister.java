package fr.zenika.zenika_emarket_v3.it;

import fr.zenika.zenika_emarket_v3.catalog.domain.Category;
import fr.zenika.zenika_emarket_v3.shop.domain.Customer;
import fr.zenika.zenika_emarket_v3.catalog.domain.Product;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * @author Emeline Hourmand
 */
@Component
public class EntitesPersister {

    private static final List<Class<?>> ENTITIES_PERSISTENCE_ORDER = List.of(
            Category.class,
            Product.class,
            Customer.class
    );

    private final EntityManager entityManager;

    public EntitesPersister(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Transactional
    public void persist(Object... entities) {
        Arrays.stream(entities)
                .sorted(Comparator.comparing(entity ->
                        ENTITIES_PERSISTENCE_ORDER.indexOf(entity.getClass())))
                .forEach(entityManager::persist);
    }
}
