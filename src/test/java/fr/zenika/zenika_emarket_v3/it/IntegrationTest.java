package fr.zenika.zenika_emarket_v3.it;

import fr.zenika.zenika_emarket_v3.ZenikaEMarketV3Application;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.test.context.TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS;
/**
 * @author Emeline Hourmand
 */
@SpringBootTest(webEnvironment = RANDOM_PORT, classes = ZenikaEMarketV3Application.class)
@ActiveProfiles({"it"})
@TestExecutionListeners(
        mergeMode = MERGE_WITH_DEFAULTS,
        listeners = { PostgresqlTestExecutionListener.class })
@Retention(RUNTIME)
@Target(TYPE)
public @interface IntegrationTest {
}
