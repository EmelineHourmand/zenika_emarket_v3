package fr.zenika.zenika_emarket_v3.it;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.TestExecutionListener;
import org.testcontainers.containers.PostgreSQLContainer;

import javax.sql.DataSource;
import java.util.Map;
import java.util.UUID;

/**
 * @author Emeline Hourmand
 */
public class PostgresqlTestExecutionListener implements TestExecutionListener {

    private final Logger LOGGER = LoggerFactory.getLogger(PostgresqlTestExecutionListener.class);

    private static final PostgreSQLContainer<?> postgresqlContainer;

    public static final String DB_NAME = "emarket" + UUID.randomUUID();
    public static final String DB_USERNAME = "emarket";
    public static final String DB_PASSWORD = "password";

    public static final ResourceDatabasePopulator DB_CLEANUP_SCRIPT =
            new ResourceDatabasePopulator(new ClassPathResource("truncate.sql"));

    static {
        postgresqlContainer = new PostgreSQLContainer<>("postgres:13.4-alpine")
                .withDatabaseName(DB_NAME)
                .withUsername(DB_USERNAME)
                .withPassword(DB_PASSWORD)
                .withTmpFs(Map.of("/var/lib/postgresql/data", ""));

        postgresqlContainer.start();

        System.setProperty("spring.datasource.url", postgresqlContainer.getJdbcUrl());
        System.setProperty("spring.datasource.username", DB_USERNAME);
        System.setProperty("spring.datasource.password", DB_PASSWORD);
    }

    @Override
    public void beforeTestMethod(TestContext testContext) {
        LOGGER.info("Initialize database before test...");
         DataSource postgresqlDatasource =
                 testContext.getApplicationContext().getBean(DataSource.class);
         DB_CLEANUP_SCRIPT.execute(postgresqlDatasource);
     }
}
