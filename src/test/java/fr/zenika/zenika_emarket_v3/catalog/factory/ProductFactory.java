package fr.zenika.zenika_emarket_v3.catalog.factory;

import fr.zenika.zenika_emarket_v3.catalog.domain.Category;
import fr.zenika.zenika_emarket_v3.catalog.domain.Product;

/**
 * @author Emeline Hourmand
 */
public class ProductFactory {

    public static Product banane(Category category) {
        Product product = new Product();
        product.setName("Banane");
        product.setDescription("Un lot de banane");
        product.setTaxInclPrice(2.5);
        product.setWtPrice(2.0);

        category.addProduct(product);

        return product;
    }

    public static Product gta5(Category category) {
        Product product = new Product();
        product.setName("GTA 5");
        product.setDescription("Un jeu Rockstar");
        product.setTaxInclPrice(60.0);
        product.setWtPrice(55.0);

        category.addProduct(product);
        return product;
    }
}
