package fr.zenika.zenika_emarket_v3.catalog.factory;

import fr.zenika.zenika_emarket_v3.catalog.domain.Category;

/**
 * @author Emeline Hourmand
 */
public class CategoryFactory {

    public static Category defaultCategory() {
        Category category = new Category();
        category.setName("Default");
        return category;
    }

    public static Category fruits() {
        Category category = new Category();
        category.setName("Fruits");
        ProductFactory.banane(category);
        return category;
    }
}
