package fr.zenika.zenika_emarket_v3.catalog;

import fr.zenika.zenika_emarket_v3.catalog.domain.Category;
import fr.zenika.zenika_emarket_v3.catalog.exception.CategoryInvalidException;
import fr.zenika.zenika_emarket_v3.catalog.factory.CategoryFactory;
import fr.zenika.zenika_emarket_v3.catalog.factory.ProductFactory;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class CategoryTest {

    @Test
    void should_create_category() {
        Category fruits = CategoryFactory.fruits();
        assertThat(fruits).isNotNull();
    }

    @Test
    void should_reject_empty_name() {
        assertThatThrownBy(() -> new Category().setName(""))
                .isInstanceOf(CategoryInvalidException.class);
    }

    @Test
    void should_return_products_list() {
        Category defaultCat = CategoryFactory.defaultCategory();
        ProductFactory.banane(defaultCat);
        ProductFactory.gta5(defaultCat);
        assertThat(defaultCat.getProducts().size()).isEqualTo(2);
    }

}