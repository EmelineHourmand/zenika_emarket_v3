package fr.zenika.zenika_emarket_v3.catalog;

import fr.zenika.zenika_emarket_v3.catalog.factory.CategoryFactory;
import fr.zenika.zenika_emarket_v3.catalog.domain.Category;
import fr.zenika.zenika_emarket_v3.catalog.factory.ProductFactory;
import fr.zenika.zenika_emarket_v3.exception.ErrorHttp;
import fr.zenika.zenika_emarket_v3.it.EntitesPersister;
import fr.zenika.zenika_emarket_v3.it.IntegrationTest;
import fr.zenika.zenika_emarket_v3.catalog.controller.dto.ProductAdminDto;
import fr.zenika.zenika_emarket_v3.catalog.controller.dto.ProductDto;
import fr.zenika.zenika_emarket_v3.catalog.domain.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Emeline Hourmand
 */
@IntegrationTest
public class ProductIt {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private EntitesPersister entitesPersister;

    @Test
    void should_return_not_found_on_unknown_product() {
        ResponseEntity<ErrorHttp> response =
                restTemplate.getForEntity("/products/{id}",
                        ErrorHttp.class, 5);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getMessage()).isNotNull();
    }

    @Test
    void should_return_existing_product() {
        Category category = CategoryFactory.fruits();
        entitesPersister.persist(category);

        ResponseEntity<Category> responseCat = restTemplate
                .getForEntity("/categories/{id}", Category.class, category.getId());

        Product product = ProductFactory.banane(category);
        product.setCategory(responseCat.getBody());
        entitesPersister.persist(product);

        ResponseEntity<ProductDto> response = restTemplate
                .getForEntity("/products/{id}", ProductDto.class, product.getId());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull()
                .returns(product.getId(), ProductDto::getId)
                .returns(product.getName(), ProductDto::getName)
                .returns(product.getTaxInclPrice(), ProductDto::getPrice)
                .returns(product.getCategory().getName(), ProductDto::getCategoryName)
                .returns(product.getCategory().getId(), ProductDto::getCategoryId);
    }

    @Test
    void should_delete_existing_product() {
        Category category = CategoryFactory.defaultCategory();

        Product product = ProductFactory.gta5(category);
        entitesPersister.persist(product, category);

        ResponseEntity<Void> deleteResponse = restTemplate
                .exchange(URI.create("/products/" + product.getId()),
                        HttpMethod.DELETE, null, Void.class);

        assertThat(deleteResponse.getStatusCode()).isEqualTo(HttpStatus.OK);

        ResponseEntity<ErrorHttp> response = restTemplate
                .getForEntity("/product/{id}", ErrorHttp.class, product.getId());
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void should_create_new_product() {
        Category category = CategoryFactory.defaultCategory();
        entitesPersister.persist(category);

        ProductAdminDto product = new ProductAdminDto()
                .setName("Fraises")
                .setDescription("Une barquette de fraises.")
                .setWtPrice(2.5)
                .setTaxInclPrice(4.0)
                .setCategoryId(category.getId());

        ResponseEntity<ProductDto> createResponse = restTemplate
                .postForEntity("/products", product, ProductDto.class);

        assertThat(createResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(createResponse.getBody().getId()).isNotNull();

        // Access to new product
        ResponseEntity<ProductDto> getResponse = restTemplate.getForEntity(
                URI.create("/products/" + createResponse.getBody().getId()),
                ProductDto.class);

        assertThat(getResponse.getBody())
                .returns(product.getName(), ProductDto::getName)
                .returns(product.getDescription(), ProductDto::getDescription)
                .returns(product.getTaxInclPrice(), ProductDto::getPrice)
                .returns(category.getId(), ProductDto::getCategoryId)
                .returns(category.getName(), ProductDto::getCategoryName);
    }

    @Test
    void should_update_product() {
        Category category = CategoryFactory.defaultCategory();
        Product banane = ProductFactory.banane(category);
        entitesPersister.persist(banane, category);

        ProductAdminDto requestDto = new ProductAdminDto()
                .setDescription("Pleins de bananes");

        restTemplate.put("/products/{id}", requestDto, banane.getId());

        ResponseEntity<ProductDto> getResponse = restTemplate.getForEntity(
                URI.create("/products/" + banane.getId()),
                ProductDto.class);

        assertThat(getResponse.getBody())
                .returns("Pleins de bananes", ProductDto::getDescription);

    }


}
