package fr.zenika.zenika_emarket_v3.catalog;

import fr.zenika.zenika_emarket_v3.catalog.factory.CategoryFactory;
import fr.zenika.zenika_emarket_v3.catalog.domain.Product;
import fr.zenika.zenika_emarket_v3.catalog.exception.ProductInvalidException;
import fr.zenika.zenika_emarket_v3.catalog.factory.ProductFactory;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;


/**
 * @author Emeline Hourmand
 */
public class ProductTest {

    @Test
    void should_create_product() {
        Product product = ProductFactory.banane(CategoryFactory.defaultCategory());
        assertThat(product).isNotNull();
    }

    @Test
    void should_reject_empty_name() {
        assertThatThrownBy(() -> new Product().setName(""))
                .isInstanceOf(ProductInvalidException.class);
    }

    @Test
    void should_reject_empty_taxInclPrice() {
        assertThatThrownBy(() -> new Product().setTaxInclPrice(null))
                .isInstanceOf(ProductInvalidException.class);
    }

    @Test
    void should_reject_empty_wtPrice() {
        assertThatThrownBy(() -> new Product().setWtPrice(null))
                .isInstanceOf(ProductInvalidException.class);
    }

}
