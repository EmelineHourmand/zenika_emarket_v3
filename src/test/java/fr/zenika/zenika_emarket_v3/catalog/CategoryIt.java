package fr.zenika.zenika_emarket_v3.catalog;

import fr.zenika.zenika_emarket_v3.catalog.factory.CategoryFactory;
import fr.zenika.zenika_emarket_v3.catalog.controller.dto.CategoryDto;
import fr.zenika.zenika_emarket_v3.catalog.domain.Category;
import fr.zenika.zenika_emarket_v3.exception.ErrorHttp;
import fr.zenika.zenika_emarket_v3.it.EntitesPersister;
import fr.zenika.zenika_emarket_v3.it.IntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Emeline Hourmand
 */
@IntegrationTest
public class
CategoryIt {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private EntitesPersister entitesPersister;

    @Test
    void should_return_not_found_on_unknown_category() {
        ResponseEntity<ErrorHttp> response = restTemplate
                .getForEntity("/categories/{id}",
                        ErrorHttp.class, 5);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getMessage()).isNotNull();
    }

    @Test
    void should_return_existing_category() {
        Category fruits = CategoryFactory.fruits();
        entitesPersister.persist(fruits);

        ResponseEntity<CategoryDto> response = restTemplate
                .getForEntity("/categories/{id}", CategoryDto.class, fruits.getId());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull()
                .returns(fruits.getId(), CategoryDto::getId)
                .returns(fruits.getName(), CategoryDto::getName);
        assertThat(response.getBody().getProductDtos()).isNotNull();
    }

    @Test
    void should_delete_existing_category() {
        Category fruits = CategoryFactory.fruits();
        entitesPersister.persist(fruits);

        ResponseEntity<Void> deleteResponse = restTemplate
                .exchange(URI.create("/categories/" + fruits.getId()),
                        HttpMethod.DELETE, null, Void.class);

        assertThat(deleteResponse.getStatusCode()).isEqualTo(HttpStatus.OK);

        ResponseEntity<ErrorHttp> response = restTemplate
                .getForEntity("/categories/{id}", ErrorHttp.class, fruits.getId());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void should_create_new_category() {
        Category newCategory = new Category();
        newCategory.setName("Jeux Vidéos");

        ResponseEntity<CategoryDto> createResponse = restTemplate
                .postForEntity("/categories", newCategory, CategoryDto.class);

        assertThat(createResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(createResponse.getBody().getId()).isNotNull();

        ResponseEntity<CategoryDto> getResponse = restTemplate
                .getForEntity(URI.create("/categories/" + createResponse.getBody().getId()),
                        CategoryDto.class);

        assertThat(getResponse.getBody()).isNotNull()
                .returns(newCategory.getName(), CategoryDto::getName);
    }

    @Test
    void should_update_category() {
        Category fruits = CategoryFactory.fruits();
        entitesPersister.persist(fruits);

        CategoryDto categoryRequest = new CategoryDto()
                .setName("Vegetables");

        restTemplate.put("/categories/{id}", categoryRequest, fruits.getId());

        ResponseEntity<CategoryDto> getResponse = restTemplate
                .getForEntity(URI.create("/categories/" + fruits.getId()),
                        CategoryDto.class);
        assertThat(getResponse.getBody())
                .returns("Vegetables", CategoryDto::getName);
    }

}
